package com.helloworld.marcelsmelo.aulaactivity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button btn = findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent it = new Intent(MainActivity.this, SegundaTela.class);
//                startActivity(it);
                EditText campoNumero = findViewById(R.id.editText2);
                Float numero = Float.parseFloat(campoNumero.getText().toString());
                //Integer.parseInt(campoNumero.getText().toString());
                Float resultado = numero * 2;

                TextView texto = findViewById(R.id.textView);
                texto.setText(resultado.toString());
            }
        });
        Log.d("Teste", "onCreate");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("Calculadora", "onStart");
    }
}
